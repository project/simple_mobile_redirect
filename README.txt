CONTENTS OF THIS FILE
---------------------

* About Simple Mobile Redirect
* Configuration

About Simple Mobile Redirect
----------------------------
Simple Mobile Redirect module is a quick and easy way to make a redirect from
a main site to a separate mobile URL/site.

Current features:

* Separate redirects for iPad, IPhone, Andriod, Opera Mini, Blackbery
* Disable redirect with a cookie set at [siteroot]/?nomobi=true
* Remove the cookie at [siteroot]/clearsimplemobileredirect (triggers a redirect
to the mobile site; rel="nofollow" recommended)
